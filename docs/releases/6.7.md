# Version 6.7
**Release Date**: 9th November 2019

* * *

This release builds on v6.6 and includes views that were previously only available in Essential Docker. Specifically,

* * *

### New Capabilities for Essential Viewer

We're introducing some new exciting features in this release which allow Viewer to become more interactive and collaborative. Commenting allows users to engage in conversations about the content they see in Viewer. Ideation, lets users identify business needs or opportunities and then submit for approval ideas which may address these, all directly from within Essential Viewer.

To enable ideation feature in Cloud and Docker complete the following steps

1.  Browse in the Repository to
    *   EA Class > EA Support > Essential Viewer > Report Configuration > Report Constant
2.  Edit the instance of Report Constant called "Ideation Enabled"
3.  Set the Report Constant Value to any value e.g. "Yes"
4.  To disable ideation features, simply set the slot value to empty (blank)

### Refreshed Header Bar

We have refreshed the design of the page header for Essential Viewer to use a icon driven menu system. It's also smaller meaning you see more of your content. We've also dropped the two tone effect for a single colour making it easier than ever to brand Essential Viewer for your organisation.

* * *

### New Views

#### Approvals Dashboard (Cloud and Docker only)

*   enterprise/core\_el\_approvals\_dashboard.xsl

#### Ideas Dashboard (Cloud and Docker only)

*   enterprise/core\_el\_bus\_need\_dashboard.xsl

#### Project KPI Tracker

*   enterprise/core\_el\_project\_kpi\_tracking.xsl

#### Technology Security Vulnerability Analysis

*   enterprise/core\_el\_security\_posture.xsl

#### Strategic Trends Dashboard

*   enterprise/core\_el\_strategic\_trends\_dashboard.xsl

#### Supplier Impact Map

*   enterprise/core\_el\_supplier\_impact\_map.xsl

#### Supplier License Management

*   enterprise/core\_el\_supplier\_license\_mgmt.xsl

#### Technology Product Lifecycle Analysis

*   technology/core\_tl\_tech\_product\_lifecycle\_analysis.xsl

* * *

### Bug Fixes and Improvements

#### ALL VIEWS

*   Added new template call to support ideation
*   `<xsl:call-template name="RenderModalReportContent"><xsl:with-param name="essModalClassNames" select="$linkClasses"/></xsl:call-template>`

#### Support both 'Application User' and 'Application Organisation User' as scoping role in the following views

*   application/core\_al\_app\_deployment\_summary.xsl
*   application/core\_al\_app\_footprint\_comparison.xsl
*   application/core\_al\_app\_provider\_summary\_v4.xsl
*   application/core\_al\_app\_ref\_model.xsl
*   enterprise/core\_el\_it\_asset\_dashboard\_checker.xsl
*   enterprise/core\_el\_it\_asset\_dashboard\_v6.xsl

#### Bug fixes in JSON generation in the following Views

*   application/core\_al\_app\_rationalisation\_analysis\_simple.xsl
*   enterprise/core\_el\_strategic\_implication\_dashboard.xsl
*   enterprise/core\_el\_strategic\_trends\_dashboard.xsl

#### Updated to support more detailed modelling

*   application/core\_al\_app\_provider\_info\_deps\_model.xsl

#### Removed external library calls in the following views

*   application/core\_al\_app\_rationalisation\_analysis\_simple.xsl
*   business/core\_bl\_bus\_cap\_to\_technology\_force.xsl
*   business/core\_bl\_bus\_cap\_to\_technology\_tree.xsl
*   enterprise/core\_el\_nist.xsl
*   integration/core\_ut\_completeness\_view.xsl
*   integration/instance\_relations\_tree.xsl

#### Switched from fixed to dynamic start date

*   enterprise/core\_el\_programme\_plan.xsl

#### Fixed an issue relating to Business Process Family

*   business/core\_bl\_bus\_process\_list\_as\_table.xsl

#### Improved details of impacts

*   enterprise/core\_el\_project\_summary.xsl

#### Improved rendering performance

*   technology/core\_tl\_tech\_ref\_model.xsl
*   technology/core\_tl\_tech\_product\_lifecycle\_json.xsl
*   technology/core\_tl\_tech\_product\_list\_by\_vendor\_simple.xsl

#### Viewer Engine

*   More robust handling of unguarded infinite loops
*   Support for Ideation features

#### New XSL-based APIs

*   application/api/core\_api\_al\_app\_cap\_modal.xsl
*   application/api/core\_api\_al\_app\_codebase\_breakdown.xsl
*   application/api/core\_api\_al\_app\_delivery\_model\_breakdown.xsl
*   application/api/core\_api\_al\_app\_prov\_modal.xsl
*   application/api/core\_api\_al\_app\_svc\_modal.xsl
*   business/api/core\_api\_bl\_bus\_cap\_modal.xsl
*   business/api/core\_api\_bl\_bus\_layer\_impacts.xsl
*   business/api/core\_api\_bl\_bus\_proc\_modal.xsl
*   business/api/core\_api\_bl\_org\_modal.xsl
*   business/api/core\_api\_bl\_site\_modal.xsl
*   common/api/core\_api\_content\_approvals\_summary.xsl
*   technology/api/core\_api\_tl\_tech\_cap\_modal.xsl
*   technology/api/core\_api\_tl\_tech\_comp\_modal.xsl
*   technology/api/core\_api\_tl\_tech\_prod\_modal.xsl

#### New Language file for Lithuanian

#### Removed specific calls for the Jquery UI library

*   Added to common\_head\_content.xsl for global usage

#### Added the following external libraries

_Dagre_

*   js/dagre

_Filepong_

*   js/filepond

_Hopscotch_

*   js/Hopscotch

_jBox_

*   js/jBox

_Jquery Yearpicker_

*   js/yearpicker

_JVectorMap_

*   Added UK map

_NouiSlider_

*   js/nouislider

_Bootstrap Star Rating_

*   js/star-rating

  

### IE11 Support

Since 2015 Microsoft IE11 has been superseded by Microsoft Edge, has had limited updates and does not support a number of modern web standards. With Microsoft actively discouraging the use of IE11, we have decided that our next release will be the last to support IE11

The implication is that some elements of the Essential platform may use features that are not supported by IE11. As a principle, we will not fix bugs that are specific to IE11 after the next release.