# Version 6.11
**Release Date**: 6th October 2020

* * *

The 6.11 release is a major update which has improvements to usability, performance and security across all components of the Essential platform.

* * *

Key Features
------------

#### New Views and Editors

*   Application Design Authority View
*   Business Process Hierarchy View
*   Enterprise Architecture Navigator View
*   Business Scenario Analysis Editor
*   Business Capability Model Editor
*   Information Reference Model Editor
*   Technology Reference Model Editor

#### Audit Log (beta) - Cloud and Docker only

Starting with 6.11, system administrators will now be able to access an Audit Log of all transaction and events across the Essential Cloud and Essential Docker platforms. You can run queries to identify what actions occurred, when they happened and who performed the action. We're launching this in beta and would welcome any feedback you have on this much-requested feature.

#### Single sign-out and working across devices & browsers - Cloud and Docker only

We are improving the way sessions are managed across the various components of Essential Cloud. Starting with version 6.11, when you sign out of one component of Essential Cloud, you will now be signed out of all components immediately. Additionally, a user will now only be allowed to be signed in to one device at a time. If a user switches device then they will be prompted to re-authenticate.

* * *

Essential Editor - Cloud and Docker only
----------------------------------------

#### New Editors

*   Business Capability Model Editor
*   Information Reference Model Editor
*   Technology Reference Model Editor
*   Business Scenario Analysis Editor

#### Bug Fixes and Improvements

*   Application Editor

*   Performance/response time improvements and UI reformatting
*   Maintaining individual and organisational stakeholders
*   Maintaining the deployment lifecycle of Applications
*   Maintaining Application costs
*   Ability to create new Technology Products directly from the Technology Usage tab
*   Hiding sections based on fine-grained user permissions.

*   Technology Product Editor

*   Bug fixes and UX improvements

*   Editor Framework

*   Ability to add HTML/JS content to Editor Headers, e.g. tracking
*   New global JS properties on essEnvironment variable

*   essEnvironment.repoName, essEnvironment.targetEditorID, essEnvironment.targetEditorPath, essEnvironment.targetEditorLabel

* * *

Essential Viewer
----------------

#### Generate PDF - Cloud and Docker only

We are introducing a PDF generation solution for Essential Viewer. This has been one of the most requested features for Essential Viewer over the past few years. The print functionality, accessible by clicking on the printer icon in the menu bar, allows to create a PDF from any View. There are options to allow you to tune the page options to ensure the resulting PDF is rendered at the correct size. At launch, there is a limitation that the PDF will only generate what the page looks like at load time. We will address this limitation in a future release. Many users will already be using their own existing browser-based PDF functionality but for where this isn't available, we believe this will be a great solution.

#### Essential Data Manager Deep Links from Essential Viewer - Cloud and Docker only

This feature allows you to jump straight to the edit page in the correct repository directly from any View in Essential Viewer. This is fully supported by the Essential security framework and only users with the correct permissions will be able to view the instance in the repository. This feature is optionally enabled using a Report Constant in the repository.

To enable this feature, enter the value "on" in the Report Constant Value slot, and to disable this feature, set the Report Constant Value slot to be blank

#### Report Caching

Data Set API report caching adds support for the pre-caching of JSON data produced using XSL by instances of the Data Set API class. This significantly improves the performance of views that rely on complex or large data sets.

On publish, the Essential Viewer will immediately begin generating and caching the data for Data Set API instances that have pre-caching enabled (check the pre-cache checkbox). This may take several minutes depending on the scope and complexity of data being cached. Note for dynamic, parameterised data sets, the pre-caching tick box should not be checked.

#### XSLT v3.0 support

As previously mentioned in our tech note in April 2020, this release updates the Saxon XSLT rendering engine to support XSLT v3.0.

#### New Views

*   Application Design Authority View
*   Business Process Hierarchy View
*   Enterprise Architecture Navigator View

#### New XSL-based REST API views

*   application/api/core\_api\_al\_application\_providers\_list.xsl
*   application/api/core\_api\_al\_buscap\_app\_pro\_roles.xsl
*   application/api/core\_api\_al\_buscap\_app\_services.xsl
*   application/api/core\_api\_al\_get\_all\_application\_providers.xsl
*   application/api/itasset\_api\_al\_application\_capabilities\_l1\_to\_services.xsl
*   application/api/itasset\_api\_al\_application\_piecharts.xsl
*   business/api/core\_api\_bl\_bus\_capability\_hierarchy\_dynamic.xsl
*   business/api/core\_api\_bl\_bus\_capability\_process\_activities.xsl
*   business/api/core\_api\_bl\_bus\_domain.xsl
*   business/api/core\_api\_views\_it\_asset\_api\_bl\_get\_business\_capabilities.xsl
*   business/api/core\_api\_views\_it\_asset\_api\_bl\_get\_business\_units.xsl
*   business/api/core\_api\_views\_trm\_api\_bl\_get\_business\_units.xsl
*   common/api/core\_precache\_reportReference.xsl
*   enterprise/api/core\_api\_el\_arch\_impact\_includes.xsl
*   enterprise/api/core\_api\_el\_impact\_scope\_data.xsl
*   enterprise/api/core\_api\_el\_impact\_scope\_data\_includes.xsl
*   enterprise/api/core\_api\_el\_strat\_trend\_dashboard\_deps.xsl
*   enterprise/api/core\_api\_el\_strat\_trends\_and\_impls.xsl
*   enterprise/api/core\_api\_el\_strategic\_trend\_arch\_impacts.xsl
*   integration/api/core\_api\_class\_info.xsl
*   technology/api/core\_api\_get\_all\_tech\_nodes\_detail.xsl
*   technology/api/core\_api\_tl\_get\_all\_technology\_suppliers.xsl
*   technology/api/core\_api\_tl\_itasset\_technology\_capabilities\_l1\_to\_products\_nos.xsl
*   technology/api/core\_api\_tl\_technology\_capability\_list.xsl
*   technology/api/core\_api\_tl\_technology\_component\_list.xsl
*   technology/api/core\_api\_tl\_technology\_component\_list\_from\_tprs.xsl
*   technology/api/core\_api\_tl\_technology\_product\_list\_with\_standards.xsl
*   technology/api/core\_api\_tl\_technology\_product\_list\_with\_supplier\_componentIDs.xsl
*   technology/api/core\_api\_tl\_technology\_product\_suppliers.xsl
*   technology/api/core\_api\_tl\_trm\_technology\_ref\_model\_summary.xsl
*   technology/api/core\_api\_views\_it\_asset\_api\_tl\_get\_all\_tech\_capability\_detail.xsl
*   technology/api/core\_api\_views\_it\_asset\_api\_tl\_get\_all\_tech\_products\_roles.xsl
*   technology/api/core\_api\_views\_it\_asset\_api\_tl\_get\_tech\_products.xsl
*   technology/api/core\_api\_views\_trm\_api\_tl\_get\_all\_tech\_products\_roles.xsl
*   technology/api/it\_asset\_api\_technology\_product\_list.xsl
*   technology/api/itasset\_api\_tl\_technology\_piecharts.xsl

#### External Access to XSL-based REST APIs (Cloud and Docker)

Cloud/Docker clients can now call XSL-based REST APIs from outside the Essential Viewer environment via OAUTH2 client workflow

#### Bug Fixes and Improvements

**Business Capability Summary** (business/core\_bl\_bus\_cap\_summary.xsl)

*   Removed and inconsistency between this and the application/core\_al\_bus\_cap\_support.xsl View. Additionally, added sub-capability selection and also added sub-process support to the process table. This shows all sub-processes supporting a process that supports the capability

**Business Process Summary** (business/core\_bl\_bus\_process\_summary.xsl)

*   Added parent process and sub-process and also infers capabilities the process supports from the parents

**Business Capability Application Fit** (business/core\_bl\_app\_cap\_mapping.xsl)

*   Fix to address issue where direct applications were not showing a fit score

**Application to Business Capability Analysis** (ext/apml/ess\_al\_bus\_cap\_support.xsl), **Business Application Footprint** (application/core\_al\_bus\_cap\_support.xsl) and **Business Capability Application Fit** (business/core\_bl\_app\_cap\_mapping.xsl)

*   Fixed an issue with the depth of business capabilities being picked up

**Application Rationalisation Analysis** (application/core\_al\_app\_rationalisation\_analysis\_simple.xsl & ext/apml/ess\_al\_app\_rationalisation\_analysis.xsl)

*   Fixed issue with multi-line names

**Technology Lifecycle Analysis** (technology/core\_tl\_tech\_product\_lifecycle\_analysis.xsl)

*   Resolved an issue where standards filtering check boxes were not working as expected and the no standard box had disappeared

**Information Representation Summary** (information/core\_il\_inf\_rep\_summary.xsl)

*   Aligned the frequency with the app dependency view

**Technology Node Catalogue** (technology/core\_tl\_tech\_node\_list\_as\_table\_api.xsl), **Technology Reference Model** (core\_tl\_tech\_ref\_model\_api.xsl) and **Project Summary** (enterprise/core\_el\_project\_summary.xsl)

*   Refactored to improve performance

**Application Provider Summary** (application/core\_al\_app\_provider\_summary.xsl)

*   Removed duplicate issues block

**Launchpad Export** (integration/l4\_launchpad\_export.xsl)

*   Fixed bug where label column was missing from the status import
*   Removed debug text from the level field in business capability sheet

**Launchpad Application Fit** (integration/l4plus\_app\_fit.xsl)

*   Fix so styles tables can have values not connected to an app

**Launchpad Strategy Planner Export** (integration/l4plus\_strat\_planner\_export.xsl)

*   L5 Strategy Planner export - added CJP to Value Stream worksheet

**Out-of-the-box Portal Template**

*   Views/Editors that a user does not have permission to read, are hidden

**Value Stream Summary**

*   Added presentation of supporting Business Capabilities against Value Stages

#### Library Changes

*   Jquery upgraded to v3.5.1
*   HandlebarsJS upgraded to v4.7.6
*   Added D3 Plus
*   Added Radar.js
*   Removed all previous versions of Jquery and HandlebarsJS

* * *

Essential Data Manager
----------------------

#### New Features

##### Instance References Panel

This allows you to see all the relationships the selected instance has to other instances in the repository

##### Audit Log

Access to the new Audit Log feature is accessible from the System Administrator page

#### Bug Fixes and Improvements

*   Autotext now updates on API events. Other general auto-text improvements
*   Search, sort and filter using new tables across all System and Repository Admin pages
*   Improved notifications - less persistent and shorter durations
*   Refined UI to allow access to more content on the screen
*   Improved error messaging
*   Lists now sort correctly for both upper and lower cases instances
*   Drag to sort in Chrome browser now scrolls as you drag
*   Text is (once again) selectable in Chrome browser

* * *

Essential Import Utility
------------------------

#### Bug Fixes and Improvements

*   Improved session management
*   Security and stability improvements

* * *

Essential REST APIs - Cloud and Docker only
-------------------------------------------

#### API security

With version 6.11 we will be introducing our clearance level security layer to the Essential API. This means the Utility and Core APIs will now check that the user making the API call has the relevant clearance levels and access rights for data in scope for the API request. If you are not using clearance levels, then the APIs will continue to function as normal. If you are using clearance levels, then you should check the user making the calls has the relevant read and edit rights for each repository. Note that this will also impact Editor users

#### New APIs

*   Information Concepts
*   Information Views
*   Information Domains
*   Individual Roles
*   Enumerations
*   Bug Fixes and improvements

#### v1 Core API endpoints no longer available

Customers using the v1 API endpoints should update their code to use the newer v2 API. The v1 APIs have been deprecated for over a year and are now being retired as part of the introduction of API security.

See the Essential API Documentation for the latest specification API documentation can be found at [https://api.enterprise-architecture.org](https://api.enterprise-architecture.org/)

* * *

APQC Process Models
-------------------

Many thanks to APQC for allowing us to provide Essential data packs for their industry specific process models. Download the DUP, import into Essential and you will have the APQC process model, which you can map to your capabilities, applications, etc. Find out more here. [https://www.enterprise-architecture.org/dupcentral.php#processModels](https://www.enterprise-architecture.org/dupcentral.php#processModels)