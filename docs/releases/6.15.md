# Version 6.15
**Release Date**: 11th February 2022

* * *

This release is a major platform release focused on delivering improved performance and greater stability.

**IMPORTANT:** All customers who use **ESSENTIAL LAUNCHPAD** need to take action. See details at the bottom of this page.

#### Key Features

*   New style Application Dependencies View
*   Improved Editor and REST API performance
*   Faster Out-of-the-Box Data Set APIs
*   Security Patches

#### Viewer

*   New style Application Dependencies View
*   New Style Group Actor Summary
*   Programme Plan - fix issue where milestone were not properly positioned
*   Lifecycle Viewer - added option to allow selection of “unknown” apps not mapped to capabilities
*   Significant performance improvements across many out-of-the-Box Data Set APIs

#### Other

*   Added successful API authentication attempts to the Audit Log
*   Security patches across all software components

#### Meta-Model

**:APU-TO-APU-STATIC-RELATION**

*   Update to the default automatic naming for the Application Provider Usage to Application Provider Usage class

**Content Quality Governance**

*   New classes in EA Support section to support new features in a forthcoming release of Essential Cloud.

**Application\_Provider\_Role**

*   Add inverse slot to Applicatuion\_Provider\_Standard\_Specification -> aps\_standard\_app\_provider\_role -> apr\_has\_standard\_specs

**Reference Architectures vs Reference Implementations**

*   Add meta-model extensions to more explicitly support reference architectures and implementations

**Business\_Capability**

*   belongs\_to\_business\_domain slot change to multi-cardinality

**Cost**

*   Add cost\_responsible\_group slot. The group actor or role that is responsible for the Cost

**Application\_Capability**

*   Add realised\_by\_application\_services slot as inverse slot to realises\_app\_capabilities slot on Application\_Service

**Application\_Provider\_Interface**

*   Updated description

**Infrastructure\_Software\_Instance**

*   Add inverse slot to technology\_instance\_of

**Report\_Constant (Default Currency)**

*   Deprecate - use slot on Currency class instead

**User Count Range**

*   New Enumeration class and slot on Application\_Provider called usetr\_count\_range

**PLAN\_TO\_ELEMENT\_RELATION**

*   Auto-text name wrong - missing a space after plan\_to\_element\_change\_action

**Application Provider to Business Relationship**

*   Remove Deprecated label from App slot

**RTO/RPO**

*   Recovery\_Time\_Objective class with slot ea\_recovery\_time\_objective on Application\_Provider\_Type
*   Recovery\_Point\_Objective class with slot ea\_recovery\_point\_objective on Application\_Provider\_Type
*   Enumerations - Recovery\_Time\_Objective, Recovery\_Point\_Objective both with the instance slot rto\_for\_business\_criticalities - Business Criticality and multilple to define the link between the application RTO and the Business Process Criticality level

**New Application Organisation Owner role**

*   To be used as a role for an app owner and to show whether app is internal or external vis the actor

**Application\_Function**

*   type\_of\_application\_capabilities CHANGE to be multi-cardinality

**Sequence Number**

*   Add the existing “sequence number” to the Application Capability, Infoformation Domain, Information Concepts, Technology Domains and Technology Capabilities classes

New Taxonomy instance for tagging Project Lifecycle Statuses

*   **Name:** SYS\_PROJECT\_LIFECYCLE\_STAGE\_TYPES
*   **Terms:** SYS\_PROJECT\_IN\_PLANNING, SYS\_PROJECT\_EXECUTION,  SYS\_PROJECT\_ENDED

* * *

#### Launch Pad Import Specification v5.8

An updated version of the Import Specification for Essential Launchpad is now available. It is important to use this version to import any Launchpad spreadsheets once you have applied the Essential Meta Model 6.15 Update Pack.

[Download Essential Launchpad v5.8 Import Specification](downloads_area/essential_launchpad_v58_import_spec.zip)

Note that if you have developed any of your own import specifications that create derived instances of the :APU-TO-APU-STATIC-RELATION class, you should review the ID and Naming patterns that you have defined to ensure that they match the updated default pattern.

This now takes the form:

**:APU-TO-APU-STATIC-RELATION**

*   ‘Static Architecture of::’
*   REF C
*   ‘: Relation from ’
*   REF C
*   ‘::in::Static Architecture of::’
*   REF C
*   ‘ to ‘
*   REF B
*   ‘::in::Static Architecture of::’
*   REF C

Derived ID and Derived Instance Name are the same and the resulting format will be:

_Static Architecture of::\[C\]: Relation from \[C\]::in::Static Architecture of::\[C\] to \[B\]::in::Static Architecture of::\[C\]_

**APP\_PRO\_TO\_INFOREP\_EXCHANGE\_RELATION**

*   ‘Static Architecture of::’
*   REF: C
*   ‘: Relation from ‘
*   REF C
*   ‘::in::Static Architecture of::’
*   REF C
*   ‘ to ‘
*   REF B
*   ‘::in::Static Architecture of::’
*   REF C
*   ‘ exchanging ‘
*   REF D
*   ‘ managed by ‘
*   REF B

Derived ID and Derived Instance Name are the same and the resulting format will be:

_Static Architecture of::\[C\]: Relation from \[C\]::in::Static Architecture of::\[C\] to \[B\]::in::Static Architecture of::\[C\] exchanging \[D\] managed by \[B\]_