# Version 6.8
**Release Date**: 1st December 2019

* * *

This is a minor release focused primarily on Essential Cloud although an update is available for Open Source which addresses some minor bugs.

* * *

### Vanity URLs for Essential Cloud

We're delighted to finally introduce Vanity URLs for Essential Cloud customers. It is now possible to access the platform using your tenant id as part of the URL e.g https://tenantname.essentialintelligence.com.

This results in an improved login experience when using vanity URL for non-SAML users by removing the need to enter a tenant ID and an improved login experience for SAML users by adding the ability to login directly for the main login page (removes second step)

* * *

### New Editor Forms

We're introducing 3 new, much-requested, Editor Forms in version 6.8

*   Business Process Editor
*   Technology Product Editor
*   Technology Type Editor

We're also adding help text to all Editors to make these powerful tools easier to get to know.

* * *

### Bug Fixes and Improvements

*   All select2.js select boxes now properly use a "Bootstrap" style
*   Support for Dynamic linking in Editor Forms
*   Home Page - Removed hard-coded View Library link
*   Business Capability to Change Footprint - Fixed bug where the wrong variable was being used to identify the applications supporting a Business Capability
*   IT Asset Dashboard - Improved load performance
*   Security Posture - Include mapping to Technology Product Family

* * *

### New Libraries

*   Input Mask - https://github.com/RobinHerbots/Inputmask
*   JS Tree - http://jstree.com/

* * *

### IE11 Support

From this release onwards, we will no longer be addressing any bugs that are specific to Microsoft Internet Explorer 11 and we strongly recommend customers move to using Essential with a modern browser such as Chrome, Edge, Firefox or Safari.