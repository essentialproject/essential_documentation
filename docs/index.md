# Essential Updates
These pages contain links to release notes and updates for all production versions of Essential Open Source and Essential Cloud. It also contains links to available supporting update packs (EUP and DUP).

* * *

<img src="images/essential_cloud_logo_2016@0.5x.png" title="essential cloud" width="200px" style="margin-right: 30px;"/><img src="images/essential_docker_logo_2019@2x.png" title="essential docker" style="margin-right: 30px;" width="200px"/><img src="images/essential_OS_logo.png" title="essential OS" width="200px" style="margin-right: 30px;"/>

* * *

### What is this repository for?

Release Notes and Update Packs

### Who do I talk to?

team@enterprise-architecture.org